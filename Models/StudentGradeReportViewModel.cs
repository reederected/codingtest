using System.Collections.Generic;

namespace CodingTest.Models
{
    public class StudentGradeReportViewModel
    {
        public decimal AverageGrade { get; set; }
        public List<GradeCount> LetterGradeCounts { get; set; }
        public List<StudentGrade> GradeList { get; set; }
    }
}
