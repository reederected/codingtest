using System.ComponentModel.DataAnnotations;

namespace CodingTest.Models
{
    public class GradeCount
    {
        [Display(Name = "Letter Grade")]
        public string LetterGrade { get; set; }

        [Display(Name = "Count")]
        public int LetterGradeCount { get; set; }
    }
}
